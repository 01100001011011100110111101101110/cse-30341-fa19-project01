/* ls.c: ls applet */

#include "idlebin.h"

#include <errno.h>
#include <libgen.h>
#include <limits.h>
#include <stdbool.h>
#include <string.h>

#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>

/* Usage */

const char LS_USAGE[] = \
    "Usage: ls [-Ra] [PATHS]...\n"
    "   -R    List subdirectories recursively\n" \
    "   -a    Do not ignore entries starting with .";

/* Global Variables */

static bool Recursive  = false;     /* -R flag */
static bool ShowHidden = false;     /* -a flag */
static int  ExitStatus = 0;         /* Track final exit status */
static bool FirstShown = true;      /* Track if first shown entry (for spacing) */

/* Function */

int pathcmp(const void *p1, const void *p2) {
    return strcoll(* (const char **) p1, * (const char **) p2);
}

void show_file(const char *path) {
    puts(path);
    FirstShown = false;
}

void show_directory(const char *path, bool title) {
    if (title) {
        printf("%s%s:\n", (FirstShown ? "" : "\n"), path);
    }

    struct dirent **entries;
    int n = scandir(path, &entries, NULL, alphasort);
    if (n < 0 ) {
        fprintf(stderr, "ls: cannot access '%s': %s\n", path, strerror(errno));
        ExitStatus = 1;
        return;
    }

    for (int i = 0; i < n; i++) {
        bool is_dotfile = entries[i]->d_name[0] == '.';
        if (is_dotfile && !ShowHidden) {
            continue;
        }
        show_file(entries[i]->d_name);
    }

    for (int i = 0; i < n; i++) {
        bool is_meta      = streq(entries[i]->d_name, ".") || streq(entries[i]->d_name, "..");
        bool is_dotfile   = entries[i]->d_name[0] == '.';
        bool is_directory = entries[i]->d_type == DT_DIR;
        bool is_hidden    = is_dotfile && !ShowHidden;

        if (!is_hidden && !is_meta && is_directory && Recursive) {
            char full_path[PATH_MAX];
            snprintf(full_path, PATH_MAX, "%s/%s", path, entries[i]->d_name);
            show_directory(full_path, true);
        }

        free(entries[i]);
    }

    free(entries);
}

void list_path(const char *path, bool title) {
    struct stat s;
    if (stat(path, &s) < 0 ) {
        fprintf(stderr, "ls: cannot access '%s': %s\n", path, strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (S_ISDIR(s.st_mode)) {
        show_directory(path, title);
    } else {
        show_file(path);
    }
}

/* Applet */

int ls_applet(int argc, char *argv[]) {
    /* Parse command line arguments */
    int argind = 1;

    while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') {
        char *arg = argv[argind++];
        for (size_t i = 1; i < strlen(arg); i++) {
            switch (arg[i]) {
                case 'R':
                    Recursive = true;
                    break;
                case 'a':
                    ShowHidden = true;
                    break;
                case 'h':
                    display_usage(LS_USAGE, EXIT_SUCCESS);
                    break;
                default:
                    display_usage(LS_USAGE, EXIT_FAILURE);
                    break;
            }
        }
    }

    /* Process each path */
    if (argind == argc) {
        list_path(".", Recursive);
    } else {
        bool title = (argc - argind > 1) || Recursive;
        qsort(&argv[argind], argc - argind, sizeof(char *), pathcmp);
        while (argind < argc) {
            list_path(argv[argind++], title);
        }
    }

    return ExitStatus;
}

/* vim: set expandtab sts=4 sw=4 ts=8 ft=c: */
