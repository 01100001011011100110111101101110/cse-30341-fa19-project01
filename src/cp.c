/* cp.c: cp applet */

#include "idlebin.h"

#include <errno.h>
#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <limits.h>   

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

/* Usage message */
const char CP_USAGE[] = \
    "Usage: cp [-Rv] SOURCE... TARGET\n"
	"   -R    Copy directories recursively\n"
	"   -v    Explain what is being done";

/*Global Variables */
static int  ExitStatus = 0;
static bool Recursive  = false;
static bool Verbose = false;

/* function to copy a file*/
int copy(char * src, char * dest)
{
	//verbose conditional
	if(Verbose){
		fprintf(stdout, "%s -> %s\n", src, dest);
	}
	/* Open source file for reading */
    int rfd = open(src, O_RDONLY);
    if (rfd < 0) 
	{ //check for sys call failure
    	fprintf(stderr, "Unable to open %s: %s\n", src, strerror(errno));
    	return EXIT_FAILURE;
    }

    /* Open destination file for writing */
    int wfd = open(dest, O_CREAT|O_WRONLY, 0644);    // Add mode
    if (wfd < 0) 
	{ //check for sys call failure
    	fprintf(stderr, "Unable to open %s: %s\n", dest, strerror(errno));
		close(rfd);
    	return EXIT_FAILURE;
    }
	
    /* Copy from source to destination */
    char buffer[BUFSIZ];
    int  nread;

    while ((nread = read(rfd, buffer, BUFSIZ)) > 0) 
	{
		//error check read
		if(nread<0)
		{
			fprintf(stderr, "Unable to read %s: %s\n", src, strerror(errno));
			close(rfd);
			close(wfd);
			return EXIT_FAILURE;
		}
        int nwritten = write(wfd, buffer, nread);
		//error check write
		if(nwritten<0)
		{
			fprintf(stderr, "Unable to write %s: %s\n", dest, strerror(errno));
			close(rfd);
			close(wfd);
			return EXIT_FAILURE;
		}
        while (nwritten != nread) 
		{                 // Write in loop
            nwritten += write(wfd, buffer + nwritten, nread - nwritten);
			if(nwritten<0)
			{
				fprintf(stderr, "Unable to write %s: %s\n", dest, strerror(errno));
				close(rfd);
				close(wfd);
				return EXIT_FAILURE;
			}	
        }
    }
	
	/*clean up */
	close(rfd);
	close(wfd);
	return EXIT_SUCCESS;
}
/* function which copies recursivley*/
int copyReccur(char * src, char * dest)
{
	//debug("directory copy src: %s, dest: %s", src, dest);
	//open directory to copy
    DIR  *d  = opendir(src);
	if (d == NULL) 
	{
    	fprintf(stderr, "Unable to opendir %s: %s\n", src, strerror(errno));
		return EXIT_FAILURE;
    }
	if(src[strlen(src)]!='/'){
		char * pat=(char *) malloc(BUFSIZ);
		strcpy(pat, dest);
		strcat(pat, "/");
		strcat(pat, src);
		mkdir(pat, 0777);
		free(pat);
	}
	//open target path 
	DIR *c = opendir(dest);
    if (c == NULL) 
	{
		closedir(d);
    	fprintf(stderr, "Unable to opendir %s: %s\n", dest, strerror(errno));
		return EXIT_FAILURE;
    }
	
    struct dirent *e;
    while ((e = readdir(d))) 
	{
		//skip current and parent directory
    	if (strcmp(".", e->d_name) == 0 || strcmp("..", e->d_name) == 0)
    	    continue;
		
		//create dest name
		char * newDest=(char *) malloc(BUFSIZ);
		stpcpy(newDest, dest);
		strcat(newDest, "/");
		strcat(newDest, e->d_name);
		//debug("new pathname: %s", newDest);
		//stat e
		struct stat statE;
		char * p=(char *) malloc(BUFSIZ);
		strcpy(p, src);
		char path[PATH_MAX + 1]; 
		strcat(p, "/");
		strcat(p, e->d_name);
		realpath(p, path);
		//debug("working on file %s", path);
		
		if(stat(path, &statE) == -1)
		{
			perror("CP");
			closedir(d);
			closedir(c);
			return EXIT_FAILURE;
		}
		//if e is a directory, then recurse down and copy it's contents:
		if(S_ISDIR(statE.st_mode))
		{	
			
			//debug("new pathname: %s", newDest);
			mkdir(newDest, 0777);
			if((copyReccur(p, newDest)!=EXIT_SUCCESS)){
				return EXIT_FAILURE;
			}
		}
		else if(copy(p, newDest)!=EXIT_SUCCESS) //else just copy it copy file to the location
		{
			return EXIT_FAILURE;
		}
		free(p);
		free(newDest);
    }
    closedir(d);
	closedir(c);
    return EXIT_SUCCESS;
	
}

int cp_applet(int argc, char *argv[]) 
{
    /* Parse command-line options */	
	if(argc <3)
	{//not enough args, then display help message
		display_usage(CP_USAGE, EXIT_FAILURE);
	}
	int argind = 1;
	while (argind < argc && strlen(argv[argind]) > 1 && argv[argind][0] == '-') 
	{
		char *arg = argv[argind++];
		switch (arg[1]) 
		{
			case 'R':
				Recursive = 1;
				break;
			case 'v':
				Verbose = true;
				break;
			case 'h':
				display_usage(CP_USAGE, EXIT_FAILURE);
			break;
			default:
				display_usage(CP_USAGE, EXIT_FAILURE);
		}
	}
	//create src and dest strings
	char * src;
	char * dest;
	//set src to the first arg after the - args
	if((argind+1)==argc){
		display_usage(CP_USAGE, EXIT_FAILURE);
	}
	src = argv[argind++];

	//set dest to the last of all args
	dest = argv[argc-1];
	/* Begin copying */
	struct stat statSrc;
	struct stat statDest;
	//stat the source. If it doesn't exist, then we kick out
	if(stat(src, &statSrc) == -1) 
	{
		fprintf(stderr, "cp: cannot stat '%s': No such file or directory\n", src);
		return EXIT_FAILURE;
	}
	bool isTargDir = false;
	
	if(stat(dest, &statDest) == -1) 
	{
		//if it does not exist and the src is a dir, then create it, then stat it again. If that fails, then return error
		if(S_ISDIR(statSrc.st_mode))
		{
			mkdir(dest, 0777);
			if(stat(dest, &statDest) == -1){
				fprintf(stderr, "cp: cannot stat '%s': No such file or directory\n", dest);
				return EXIT_FAILURE;
			}
			isTargDir = true;
			debug("target_is_directory: 1");
		}
		else
		{
			debug("target_is_directory: 0");
		}
	}
	else
	{
	//if it does exist, check if directory or not
	if(S_ISDIR(statDest.st_mode))
	{
		isTargDir = true;
		debug("target_is_directory: 1");
	}
	else
	{
		debug("target_is_directory: 0");
	}
	}
	

	//check if target is directory or not
	/* copy, either recursively or not*/
	if(Recursive)
	{
	
		//check if the target file is a directory:
		if(S_ISDIR(statSrc.st_mode))
		{
			if(isTargDir)
			{
				debug(" source_path = %s, target_path = %s", src, dest);
				ExitStatus=copyReccur(src, dest);
			}
			else
			{
				fprintf(stderr, "cp: cannot overwrite non-directory '%s' with directory '%s'", dest, src);
			}
		}
		else
		{
			if(isTargDir)
			{
				//if we are targeting a file directory, we concat the dest with a / and then the src name to copy the file
				strcat(dest, "/");
				strcat(dest, src);
				debug(" source_path = %s, target_path = %s", src, dest);
				strcat(dest, src);
				ExitStatus=copy(src, dest);
			}
			else
			{
				ExitStatus=copy(src, dest);
			}
		}
	}
	else
	{
		if(S_ISDIR(statSrc.st_mode))
		{
			fprintf(stderr, " -R not specified; omitting directory '%s'", src);
		}
		else
		{
			if(isTargDir)
			{
				//if we are targeting a file directory, we concat the dest with a / and then the src name to copy the file
				strcat(dest, "/");
				strcat(dest, src);
				debug(" source_path = %s, target_path = %s", src, dest);
				ExitStatus=copy(src, dest);
			}
			else
			{
				//if not, then just use regular copy
				debug(" source_path = %s, target_path = %s", src, dest);
				ExitStatus=copy(src, dest);
			}
		}
	}
	return ExitStatus;

}