# Project 01: Idlebin

This is [Project 01] of [CSE-30341-FA19].

## Student

- Domer McDomerson (dmcdomers@nd.edu)


## Brainstorming

To a look at the provided implementation of `ls` and answer the following questions:



1. How are command line arguments handled?

    
> By looping through them in a while loop and using a switch statement to set appropiate flags

2. Which system calls are used?

> exit and stat


3. How are directories traversed?
    
    
> directories are transvered in a for loop, listing each entry as they're encountered. If recursive is set to true, then when a sub-directorty is encountered, then the show_directory function is also called on the sub directory. Otherwise they  

.

Read the [cp] manpage and answer the following questions:


1. What does the `-R` flag mean? the `-v` flag?
    
    
> 


-R means that the copy should be recursive, which must be turned on if directories are to be copied. -v means it should be verbose, meaning it should tell the user what is happening


2. How would you handle copying from different types of sources such as a file?
   a directory? a link?
    
    
> Files can simply be copied using the read and write system calls, reading from one file to another. Directories would have to be transvered in some sort of loop, and the contents copied, and when sub-directoires are encountered, those would have to be recursivley looped through andf their contnets copied as well. Links would have to be expanded to their full paths, then copied according to the nature of the full path

3. How would you handle copying to different types of targets such as a file? a
 directory?
    
    
>For a file target, one would simply have to first ensure that the source is a compatible file type (i.e. not something like a directory), and if so, simply read from the source to the target. For a directory target, a copy of the source would have to be copied into it. 

.

[cp]: http://man7.org/linux/man-pages/man1/cp.1.html

## Applets

### Provided

- [x] `basename NAME [SUFFIX]`

- [x] `cat [-E] [FILES]...`

- [x] `false`

- [x] `ls [-Ra] [PATHS]...`

- [x] `pwd`

- [x] `true`

- [x] `yes [STRING]...`

### Required

- [ ] `cp [-Rv] SOURCE... TARGET`

### Optional

- [ ] `chmod OCTAL-MODE FILE...`

- [ ] `ln [-sv] TARGET LINK_NAME`

- [ ] `mkdir [-v] DIRECTORY...`

- [ ] `rmdir [-v] DIRECTORY...`

- [ ] `touch [FILES]...`


## Reflection



1. Describe the overall design and implementation of your implementation of
   `cp`.  In particular, explain what system calls you utilized and how you
 handle errors or edge cases related to system call failure.

> The system calls used were stat, open, read and write. Stat was used extinsivley, testing for things such as existence, the nature of a file (directory or not), etc. Read, write and open were used only when copying files. The source file was opened, then the target, and the the source was read from, and the target written to. Both of these system calls were done in a loop to ensure complete writeover. All syscalls where error checked since all could fail, and when an error occured, the user was notified and the program exited. 

2. Select one of the system calls you identified in the previous example, and
   describe what happens when `cp` utilizes that system call.  In particular,
   explain the transition between user application mode to operating system
   kernel mode.
    
    
> For open, the program would reach the system call, and the user app would then record the call, then record what file it wishes to open, and any other options set, then trigger and OS trap. Once that was triggered, the program would exit user mode, and the CPU would consult the trap table. The os would then transistion to kernel mode, and the OS would handle the call. In the case of open, this would mean opening the
file in the manner requested. If it could, then it would assign a file descriptor, which it would return to the program. If not, it would return -1 and set errno. In either case, it would then transistion back to user mode after this

[Project 01]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa19/project01.html
[CSE-30341-FA19]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa19/

